import qualified Utility       as Util
import qualified System.Random as Rand
import qualified System.Exit   as Exit

data GMode = Easy | Medium | Hard | Exit | Invalid deriving (Show, Read, Eq)
type Word  = String

----------------------------------------------------------------------------------------------------
--               Main method. All other methods and executions are called from here.              --
----------------------------------------------------------------------------------------------------
main = do
    name <- get_username
    putStrLn $ "So your name is " ++ name ++ "." ++ " Welcome to hangman."
    
    user_input_func name
    
    
----------------------------------------------------------------------------------------------------
--                                   ---Main method end---                                        --
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- print_menu function -- Prints the main menu for this application.
----------------------------------------------------------------------------------------------------
-- Sample input/output: call: print_menu result: Menu is printed
----------------------------------------------------------------------------------------------------

print_menu::IO()
print_menu = do
    putStrLn "1) Easy"
    putStrLn "2) Medium"
    putStrLn "3) Hard"
    putStrLn "4) Exit"

----------------------------------------------------------------------------------------------------
-- get_game_mode function. Takes in a string which has been parsed by the to_lower_string method.
-- Once taken in, it parses it to the Mode.
----------------------------------------------------------------------------------------------------
-- Sample input/output: input: get_game_mode "easy" output: Easy
----------------------------------------------------------------------------------------------------

get_game_mode::String->GMode
get_game_mode choice
    | (choice == "easy")   || (choice == "e")  = Easy
    | (choice == "medium") || (choice == "m")  = Medium
    | (choice == "hard")   || (choice == "h")  = Hard
    | (choice == "exit")   || (choice == "ex") = Exit
    | otherwise                                = Invalid

----------------------------------------------------------------------------------------------------
-- get_words function. Takes in a GMode, Easy, Medium or Hard, then returns a list of words based on
-- the difficulty level/GMODE put in.
----------------------------------------------------------------------------------------------------
-- Sample input/output: input: get_words Easy output: [Some_list_of_short_words]
----------------------------------------------------------------------------------------------------

get_words::GMode-> IO [Word]
get_words game_mode
    | game_mode == Easy   = Util.get_raw_data "Easy.txt"
    | game_mode == Medium = Util.get_raw_data "Medium.txt"
    | game_mode == Hard   = Util.get_raw_data "Hard.txt"

----------------------------------------------------------------------------------------------------
-- Get_username function. Takes in nothing and obtains a username. A username being the string value
-- the user puts in when this application is first launched.
----------------------------------------------------------------------------------------------------
-- Sample input/output: input: get_username output: [User's name]
----------------------------------------------------------------------------------------------------    

get_username::IO String
get_username = do
    putStrLn "Hello user, please input your name!"
    name <- getLine
    return $ name

----------------------------------------------------------------------------------------------------
-- get_difficulty function. Takes in nothing and uses a do block to recursively obtain user input
-- until they input a valid choice. When a valid choice is obtained it is returned.
----------------------------------------------------------------------------------------------------
-- Sample input/output: input: get_username output: [User's name]
----------------------------------------------------------------------------------------------------    

get_difficulty::IO String
get_difficulty = do
    print_menu
    putStrLn "Please select a game mode"
    
    selection <- getLine
    let p_selection = (get_game_mode . Util.get_lower_string) $ selection
    if  p_selection == Invalid
        then do
            putStrLn "Error! Invalid choice!"
            get_difficulty
        else
        if p_selection == Exit
            then
            Exit.exitSuccess
            else
            return $ selection

----------------------------------------------------------------------------------------------------
-- user_input_func is a recursive function that gets the user's desired difficulty and starts the 
-- recursive game loop with this selection in mind.
----------------------------------------------------------------------------------------------------
-- Sample input/output: start_game "some_name" Expected happenings: Game loop begins
----------------------------------------------------------------------------------------------------

user_input_func::String->IO ()
user_input_func name = do
    
    menu_choice <- get_difficulty
    gen <- Rand.getStdGen
    
    let l_menu_choice = Util.get_lower_string menu_choice
        game_mode     = get_game_mode l_menu_choice
    
    game_words   <- get_words game_mode
    
    let (index, _) = Rand.randomR(1,length game_words - 1) gen::(Int, Rand.StdGen)  
        word         = game_words!!index
        morphed_word = Util.check_letter_presence word '¬' []
    
    start_game word morphed_word []
    Rand.newStdGen    
    user_input_func name

----------------------------------------------------------------------------------------------------
-- start_game method is the main game "loop". I use that word cavalier manner since it is
-- essentially a bunch of recursive calls. If the user inputs a character the first if then is
-- triggered, otherwise the entire word is compared against their input.
----------------------------------------------------------------------------------------------------
-- Sample input/output: start_game Expected happenings: execution of game begins
----------------------------------------------------------------------------------------------------  

start_game::Word->Word->[Int]->IO ()
start_game actual_word morphed_word index_list
    | actual_word == morphed_word = do putStrLn "You won!"
    | otherwise = do
    putStrLn $ "Your word is: " ++ morphed_word
    user_input <- getLine
    if length (user_input) == 1
        then do
            let user_word      = Util.check_letter_presence actual_word (head user_input) index_list
                new_index_list = Util.get_index_list user_word
            start_game actual_word user_word new_index_list
        else
        if actual_word == user_input
            then do
                start_game actual_word actual_word index_list
            else
            do
                putStrLn "Incorrect!"
                start_game actual_word morphed_word index_list
