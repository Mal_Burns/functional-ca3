module Utility(get_lower_string
              ,check_letter_presence
              ,guess_or_check
              ,get_raw_data
              ,get_index_list) where
import qualified Data.Char as Char
import qualified System.IO as SysIO
import qualified Data.List as List

type Word    = String
type FName   = String
type Letter  = Char

data Attempt = Guess | Check deriving (Show, Read)

----------------------------------------------------------------------------------------------------
-- To lower function. Takes in a Word and returns a lower
-- case version of the word.
----------------------------------------------------------------------------------------------------
-- Sample input/output: get_lower_string "TeSt" output: "test"
----------------------------------------------------------------------------------------------------
get_lower_string::Word->Word
get_lower_string = map Char.toLower

----------------------------------------------------------------------------------------------------
-- check_letter_presence takes in a word, a letter, and a list of indexes. These are then used to
-- rewrite the word such that the characters in the index list given are returned the same, in the
-- same place and anything else that is NOT the letter that was given is returned as an underscore
-- instead.
----------------------------------------------------------------------------------------------------
-- Sample input/output: check_letter_presence "test" 'e' [0] output: "te__"
----------------------------------------------------------------------------------------------------

check_letter_presence::Word->Letter->[Int]->Word
check_letter_presence [] _ _ = error "Incorrect call of check_letter_presence function!"
check_letter_presence (x:xs) letter index_list = check_pre_with_count (x:xs) letter index_list 0
    where
    check_pre_with_count::Word->Letter->[Int]->Int->Word
    check_pre_with_count [] _ _ _ = []
    check_pre_with_count (x:xs) letter index_list count
        | (x == letter) = x : check_pre_with_count xs letter index_list (count+1)
        | otherwise     = if(count `elem` index_list)
            then
            x   : check_pre_with_count xs letter index_list (count+1)
            else
            '_' : check_pre_with_count xs letter index_list (count+1)

----------------------------------------------------------------------------------------------------
-- get_index_list takes in a word and returns a list of indexes. The purpose of this is to identify
-- where exactly there are characters other than underscore in a word. This way those can be ignored
-- when the word is checked against userinput in check_letter_presence.
-- In this way, characters that a user previously guessed can be ignored.
----------------------------------------------------------------------------------------------------
-- Sample input/output: check_letter_presence "_test_" output: [1,2,3,4]
----------------------------------------------------------------------------------------------------

get_index_list::Word->[Int]
get_index_list []     = error "Incorrect call of get_index_list!"
get_index_list word   = get_index_list_count word 0
    where
    get_index_list_count [] _ = []
    get_index_list_count (x:xs) count
        | x /= '_'  = count : get_index_list_count xs (count+1)
        | otherwise = get_index_list_count xs (count+1)
----------------------------------------------------------------------------------------------------
-- Guess or check function takes in a word and returns an Attempt. Attempts are classified as
-- Guesses or checks. A Guess is when a user attempts to guess the whole word, whereas a check is
-- when a user attempts to guess whether or not a single letter is present.
----------------------------------------------------------------------------------------------------
-- Sample input/output : guess_or_check "test" output: Guess
----------------------------------------------------------------------------------------------------

guess_or_check::Word->Attempt
guess_or_check []     = error "Error, guess_or_check called with empty list"
guess_or_check word	
    | length word > 1 = Guess
    | otherwise       = Check
    
----------------------------------------------------------------------------------------------------
-- get_raw_data takes in a FName which is a string, ie a FILE-NAME, and returns a list of the words
-- found in that file. 
----------------------------------------------------------------------------------------------------
-- Sample input/output: input: get_raw_data "Easy.txt" output: [Some_list_of_short_words]
----------------------------------------------------------------------------------------------------

get_raw_data::FName->IO [Word]
get_raw_data file_name = fmap lines $ SysIO.readFile $ "word_data/" ++ file_name
